import { Component, OnInit } from '@angular/core';
import { User } from './models/user';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from './services/user.service';
import { GLOBAL } from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [UserService]
})
export class AppComponent implements OnInit {
  title = 'Datero Activo';
  public user: User;
  public user_register: User;
  public alertRegister: string;
  public identity;
  public token;
  public errorMessage;
  public url: string;

  constructor(private _userService: UserService,
            private _route: ActivatedRoute,
             private _router: Router
            ) {
this.user = new User('', '', '', '', '', 'ROLE_USER', '', '');
this.user_register = new User('', '', '', '', '', 'ROLE_USER', '', '');
this.url = GLOBAL.url;
  }

  ngOnInit() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  public onSubmit() {
    // Conseguir los datos del usuario identificado
    this._userService.signup(this.user)
        .subscribe(response => {
            const identity = response.user;
            this.identity = identity;

            if (!identity._id) {
              alert('El usuario no esta correctamente identificado');
            }else {
              // Crear elemento en el localstorage para guardar al usuario en sesison
              localStorage.setItem('identity', JSON.stringify(identity));
            // Conseguir el token para enviarselo  a cada peticion HTTP

            this._userService.signup(this.user, 'true')
                // tslint:disable-next-line:no-shadowed-variable
                .subscribe( response => {
                    const token = response.token;
                    this.token = token;

                    if ( this.token.length <= 0 ) {
                      alert('EL token no se genero correctamente');
                    }else {
                      // Crear elemento en el localstorage para tenr token disponible
                      localStorage.setItem('token', token);
                      this.user = new User('', '', '', '', '', 'ROLE_USER', '', '');
                    }

                }, error => {
                  const errorExist = <any>error;
                  if (error != null) {
                    const body = JSON.parse(errorExist._body);
                    this.errorMessage = body.message;
                    console.log(error);
                  }
                });

            }

        }, error => {
          const errorExist = <any>error;
          if (error != null) {
            const body = JSON.parse(errorExist._body);
            this.errorMessage = body.message;
            console.log(error);
          }
        });
  }

  logout() {
    localStorage.removeItem('identity');
    localStorage.removeItem('token');
    this.identity = null;
    this.token = null;
    this._router.navigate(['/']);
  }

  onSubmitRegister() {
    console.log(this.user_register);

    this._userService.register(this.user_register).subscribe(
      response => {
        const user = response.user;
        this.user_register = user;

        if (!user._id) {
        this.alertRegister = 'Error al registrarse';
        }else {
          this.alertRegister = 'Registro se ha realizado correctamente, identificate con ' + this.user_register.email;
          this.user_register = new User('', '', '', '', '', 'ROLE_USER', '', '');
        }
      }, error => {
        const errorExist = <any>error;
        if (error != null) {
          const body = JSON.parse(errorExist._body);
         this.alertRegister = body.message;
          console.log(error);
        }
      });
}

}
