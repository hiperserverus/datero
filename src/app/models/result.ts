export class Result {
	constructor(
		public _id: string,
		public hora: string,
		public fecha: string,
		public status: string,
		public animal: string
		){}
}