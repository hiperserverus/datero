export class Ticket{
	constructor(
		public _id: string,
		public serial: string,
		public hora: string,
		public fecha: string,
		public status: string,
		public user: string
		){}
}