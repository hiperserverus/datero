export class Play{
	constructor(
		public _id: string,
		public hora: string,
		public fecha: string,
		public status: string,
		public animal: string,
		public ticket: string
		){}
}