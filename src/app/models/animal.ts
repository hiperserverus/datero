export class Animal {
	constructor(
		public _id: string,
		public name: string,
		public number: string,
		public image: string
		){}
}