import {  Component, OnInit } from '@angular/core';

import { GLOBAL } from '../services/global';
import {  UserService } from '../services/user.service';
import {  User  } from '../models/user';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'user-edit',
  templateUrl: '../views/user-edit.html',
  providers: [UserService]
})

export class UserEditComponent implements OnInit{

  public title: string;
  public user: User;
  public identity;
  public token;
  public alertMessage: string;
  public filesToUpload: Array<File>;
  public url: string;

  constructor( private _userService: UserService) {
    this.title = 'Actualizar mis datos';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.user = this.identity;
    this.url = GLOBAL.url;
  }

  ngOnInit() {
    console.log('UserEditComponent cargado');
    console.log(this.identity);
  }

  onSubmitUpdate() {

    this._userService.updateUser(this.user).subscribe(
      response => {
        if (!response.user) {
          this.alertMessage = 'El usuario no se ha actualizado';
        }else {
          // this.user = response.user;
          localStorage.setItem('identity', JSON.stringify(this.user));
          document.getElementById('identity_name').innerHTML = this.user.name;

          if (!this.filesToUpload) {
            // Redireccionar
          }else {
            this.makeFileRequest(this.url + 'upload-image/' + this.user._id, [], this.filesToUpload).then(
              (result: any) => {
                this.user.image = result.image;
                localStorage.setItem('identity', JSON.stringify(this.user));
                const image_path = this.url + 'get-image/' + this.user.image;
                document.getElementById('image-logged').setAttribute('src', image_path);

                console.log(this.user);
              }
            );
          }

          this.alertMessage = 'Datos actualizados correctamente';
        }
      }, error => {
        const errorExist = <any>error;
        if (error != null) {
          const body = JSON.parse(errorExist._body);
          this.alertMessage = body.message;
          console.log(error);
        }
      }
    );
  }

  fileChangeEvent(fileInput: any) {
    this.filesToUpload = <Array<File>>fileInput.target.files;
    console.log(this.filesToUpload);
  }

  makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
    const token = this.token;

    return new Promise(function(resolve, reject){
      const formData: any = new FormData();
      const xhr = new XMLHttpRequest();

      for (let i = 0; i < files.length; i++) {
        formData.append('image', files[i], files[i].name);
      }

      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response));
          }else {
            reject(xhr.response);
          }
        }
      };

      xhr.open('POST', url, true);
      xhr.setRequestHeader('Authorization', token);
      xhr.send(formData);

    });
  }
}
