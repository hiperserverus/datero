import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { GLOBAL } from '../services/global';
import { UserService } from '../services/user.service';
import { Result } from '../models/result';

@Component({
    selector: 'result-list',
    templateUrl: '../views/result-list.html',
    providers: [UserService]
})

export class ResultListComponent implements OnInit {
    public titulo: string;
    public results: Result[];
    public identity;
    public token;
    public url: string;

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _userService: UserService
    ) {
        this.titulo = 'Resultados';
        this.identity = this._userService.getIdentity();
        this.token = this._userService.getToken();
        this.url = GLOBAL.url;
    }

    ngOnInit() {
        console.log('result-list.component.ts cargado...');
    }
}