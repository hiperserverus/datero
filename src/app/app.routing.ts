import {  ModuleWithProviders } from "@angular/core";
import {  Routes, RouterModule  } from "@angular/router";
import {  UserEditComponent } from "./components/user-edit.component";
import {  ResultListComponent } from "./components/result-list.component";

const appRoutes: Routes = [
  {
    path: '' ,
    redirectTo: '/results/1',
    pathMatch: 'full'
  },
  { path: '', component: ResultListComponent  },
  { path: 'results/:page', component: ResultListComponent  },
  { path:'user/edit', component:UserEditComponent },
  { path:'**', component:ResultListComponent}

];

export const appRoutingProviders : any[] = [];
export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);
